import React, { Component } from "react";
import { View } from "react-native";

import CityName from "./components/CityName";
import Temperature from "./components/Temperature";
import Pressure from "./components/Pressure";
import Sun from "./components/Sun";
import SearchCity from "./components/SearchCity";
import Wind from "./components/Wind";

import styleApp from "./style/styleApp.js";

const APIkey = "324d0af06bc8371911cfd9e86146b15c";

export default class App extends Component {
  state = {
    value: ``,
    cityName: ``,
    date: ``,
    temp: ``,
    pressure: ``,
    sunset: ``,
    sunrise: ``,
    wind: ``,
    weather: [],
    err: false
  };

  handleCitySearch = e => {
    this.setState({ value: e });
  };

  handleCitySubmit = () => {
    const API = `http://api.openweathermap.org/data/2.5/weather?q=${
      this.state.value
    }&APPID=${APIkey}`;
    fetch(API)
      .then(response => {
        if (response.ok) return response;
        throw Error(response.status);
      })
      .then(response => response.json())
      .then(data => {
        const time = new Date().toLocaleString();
        this.setState(prevState => ({
          cityName: prevState.value,
          date: time,
          temp: data.main.temp,
          pressure: data.main.pressure,
          sunset: data.sys.sunset,
          sunrise: data.sys.sunrise,
          wind: data.wind.speed,
          weather: data.weather[0].description,
          err: false
        }));
      })
      .catch(err => {
        this.setState(prevState => ({
          err: true,
          cityName: prevState.cityName
        }));
      });
  };

  componentDidMount() {
    const API = `http://api.openweathermap.org/data/2.5/weather?q=Białystok&APPID=${APIkey}`;
    fetch(API)
      .then(response => {
        if (response.ok) return response;
        throw Error(response.status);
      })
      .then(response => response.json())
      .then(data => {
        const time = new Date().toLocaleString();
        this.setState(prevState => ({
          cityName: "Białystok",
          date: time,
          temp: data.main.temp,
          pressure: data.main.pressure,
          sunset: data.sys.sunset,
          sunrise: data.sys.sunrise,
          weather: data.weather[0].description,
          wind: data.wind.speed,
          err: false
        }));
      })
      .catch(err => {
        this.setState(prevState => ({
          err: true,
          cityName: prevState.cityName
        }));
      });
  }

  render() {
    const {
      value,
      cityName,
      temp,
      pressure,
      sunset,
      sunrise,
      wind,
      weather
    } = this.state;
    return (
      <View style={styleApp.container}>
        <CityName cityName={cityName} />
        <Temperature temp={temp} weather={weather} />
        <Pressure pressure={pressure} />
        <Wind wind={wind} />
        <Sun sunset={sunset} sunrise={sunrise} weather={wind} />
        <SearchCity
          searchCity={this.handleCitySearch}
          submitCity={this.handleCitySubmit}
          city={value}
        />
      </View>
    );
  }
}
