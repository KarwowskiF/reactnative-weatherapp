import React from "react";
import { TextInput } from "react-native";

import styleSearchCity from "../style/styleSearchCity";

const SearchCity = props => {
  return (
    <TextInput
      style={styleSearchCity.input}
      placeholder="Szukaj ..."
      onChangeText={props.searchCity}
      value={props.cityName}
      onSubmitEditing={props.submitCity}
    />
  );
};

export default SearchCity;
