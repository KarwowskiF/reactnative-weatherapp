import React from "react";
import { Text } from "react-native";

import styleCityName from "../style/styleCityName";

const CityName = props => {
  return <Text style={styleCityName.cityName}>{props.cityName}</Text>;
};

export default CityName;
