import React from "react";
import { Text } from "react-native";

import styleWind from "../style/styleWind";

const Wind = props => {
  return <Text style={styleWind.wind}>Prędkość wiatru {props.wind} m/s</Text>;
};
export default Wind;
