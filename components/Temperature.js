import React from "react";
import { View, Text } from "react-native";

import styleTemperature from "../style/styleTemperature";

const Temperature = props => {
  const { container, temp, weather } = styleTemperature;
  return (
    <View style={container}>
      <Text style={temp}>{(props.temp - 273.15).toFixed(1)}&deg;C</Text>
      <Text style={weather}>({props.weather})</Text>
    </View>
  );
};

export default Temperature;
