import React from "react";
import { Text } from "react-native";

import stylePressure from "../style/stylePressure";

const Pressure = props => {
  return (
    <Text style={stylePressure.pressure}>Ciśnienie {props.pressure} hPa</Text>
  );
};
export default Pressure;
