import React from "react";
import { View, Text } from "react-native";

import styleSun from "../style/styleSun";

const Sun = props => {
  const sunriseTime = new Date(props.sunrise * 1000).toLocaleTimeString();
  const sunsetTime = new Date(props.sunset * 1000).toLocaleTimeString();

  const riseHour = sunriseTime.slice(0, 2);
  const riseMin = sunriseTime.slice(3, 5);
  const setHour = sunsetTime.slice(0, 2);
  const setMin = sunsetTime.slice(3, 5);

  return (
    <View style={styleSun.container}>
      <View style={styleSun.time}>
        <Text style={styleSun.hour}>{riseHour}</Text>
        <Text style={styleSun.min}>{riseMin}</Text>
      </View>
      <View style={styleSun.time}>
        <Text style={styleSun.hour}>{setHour}</Text>
        <Text style={styleSun.min}>{setMin}</Text>
      </View>
    </View>
  );
};
export default Sun;
