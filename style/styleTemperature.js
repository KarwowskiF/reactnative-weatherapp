import { StyleSheet } from "react-native";

const styleTemperature = StyleSheet.create({
  container: {
    height: 80,
    alignItems: "center",
    justifyContent: "center"
  },
  temp: {
    fontSize: 25
  },
  weather: {
    fontSize: 14,
    color: "#aaa"
  }
});

export default styleTemperature;
