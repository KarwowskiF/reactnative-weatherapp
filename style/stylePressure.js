import { StyleSheet } from "react-native";

const stylePressure = StyleSheet.create({
  pressure: {
    fontSize: 25
  }
});

export default stylePressure;
