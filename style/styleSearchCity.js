import { StyleSheet } from "react-native";

const styleSearchCity = StyleSheet.create({
  input: {
    fontSize: 25,
    backgroundColor: "#fff",
    minWidth: 300,
    paddingLeft: 20,
    marginTop: 30,
    elevation: 4
  }
});

export default styleSearchCity;
