import { StyleSheet } from "react-native";

const styleApp = StyleSheet.create({
  container: {
    flex: 1,
    paddingTop: 100,
    paddingBottom: 100,
    alignItems: "center",
    color: "#000",
    backgroundColor: "#ddd"
  }
});

export default styleApp;
