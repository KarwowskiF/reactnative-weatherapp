import { StyleSheet } from "react-native";

const styleSun = StyleSheet.create({
  container: {
    height: 200,
    flexDirection: "row",
    width: "80%",
    justifyContent: "space-around",
    alignItems: "flex-end"
  },
  time: {
    flexDirection: "row",
    height: "35%"
  },
  hour: { fontSize: 45 },
  min: { fontSize: 23 }
});

export default styleSun;
