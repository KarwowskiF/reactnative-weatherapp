import { StyleSheet } from "react-native";

const styleCityName = StyleSheet.create({
  cityName: {
    height: 75,
    fontSize: 55,
    fontFamily: "Roboto-Bold"
  }
});

export default styleCityName;
